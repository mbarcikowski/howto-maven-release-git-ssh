# Windows #

* Generate private key with puttyGen
* Save private key somewhere
* Add public key to github/bitbucket/gitlab
* Launch pageant and add private key previously stored somewhere
* Set Environment variable : GIT_SSH=C:\Program Files (x86)\PuTTY\plink.exe     
* Configure your POM (See the [pom.xml](pom.xml) of this project as example)
* Test with mvn scm:update
* Test with mvn release:prepare
* Do mvn release:perform if you have configured distributionManagement section of your project's POM (not the case in my how-to) 
